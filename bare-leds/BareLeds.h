// -*- mode: c++; c-basic-offset: 2; -*-
class BareLeds {
public:
  BareLeds (int a, int b, int c, int d, int e, int f, int j, int n, int com1, int com2);
  void loop ();
  void show (int x);
private:
  int SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_J, SEG_N, COM_1, COM_2;
  char coms[2];
  char buf[2];
  int pins[8];
  void fun (int seg, int mask, int digit);
};
