// -*- mode: c++; c-basic-offset: 2; -*-
#include "BareLeds.h"

BareLeds l(5, 6, 8, 9, 4, 2, 3, 7, A3, A2);

#define DEBUG_TIMING 0
#if DEBUG_TIMING
long t0;
#endif

int n;

void setup() {
#if DEBUG_TIMING  
  Serial.begin (115200);
  t0 = millis ();
#endif
  n = 0;
}

void loop() {
  l.loop ();
  n++;
#if DEBUG_TIMING  
  long t1=millis ();
  if (t1 - t0 > 1000) {
    t0 = t1;
    Serial.println (n);
    n=0;
  }
#endif
}

