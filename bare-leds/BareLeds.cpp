// -*- mode: c++; c-basic-offset: 2; -*-
#include "BareLeds.h"
#include "Arduino.h"

// *     o        *  *  o  *
// F  P  1  G  H  N  A  2  B
//---------------------------
//  
//   ---A---     ---A---
//  |\  |  /|   |\  |  /|
//  F P G H B   F P G H B
//  |  \|/  |   |  \|/  |
//   -N- -J-     -N- -J-
//  |  /|\  |   |  /|\  |
//  E M L K C   E M L K C
//  |/  |  \|   |/  |  \|
//   ---D---  Q  ---D---  Q
//  
//---------------------------
// E  M  x  L  K  J  D  Q  C
// *              *  *     *
//
// 1 and 2 are common -
// Don't go past 40mA or they burn up
//
//    01
//  20  02
//   4080  
//  10  04
//    08

static const uint8_t digitFont[10] = {
  0x3f, 0x06, 0xff^0x24, 0xff^0x30, 0xff^0x19,
  0xff^0x12, 0xff^0x02, 0x7, 0xff, 0xff^0x10 };

BareLeds::BareLeds (int a, int b, int c, int d, int e, int f, int j, int n, int com1, int com2) :
  SEG_A(a), SEG_B(b), SEG_C(c), SEG_D(d), SEG_E(e), SEG_F(f), SEG_J(j), SEG_N(n), COM_1(com1), COM_2(com2)
{
  // Positive side, Anode, disabled
  pinMode (SEG_A, OUTPUT); digitalWrite (SEG_A, 0);
  pinMode (SEG_B, OUTPUT); digitalWrite (SEG_B, 0);
  pinMode (SEG_C, OUTPUT); digitalWrite (SEG_C, 0);
  pinMode (SEG_D, OUTPUT); digitalWrite (SEG_D, 0);
  pinMode (SEG_E, OUTPUT); digitalWrite (SEG_E, 0);
  pinMode (SEG_F, OUTPUT); digitalWrite (SEG_F, 0);
  pinMode (SEG_J, OUTPUT); digitalWrite (SEG_J, 0);
  pinMode (SEG_N, OUTPUT); digitalWrite (SEG_N, 0);
  // Negative side, Cathode, disabled
  pinMode (COM_1, OUTPUT); digitalWrite (COM_1, 1^1); // Transistors inverted
  pinMode (COM_2, OUTPUT); digitalWrite (COM_2, 1^1);

  coms[0] = COM_1;
  coms[1] = COM_2;
  buf[0] = 9;
  buf[1] = 8;
  pins[0] = SEG_A; pins[1] = SEG_B;
  pins[2] = SEG_C; pins[3] = SEG_D;
  pins[4] = SEG_E; pins[5] = SEG_F;
  pins[6] = SEG_J; pins[7] = SEG_N;
}

void BareLeds::fun (int seg, int mask, int digit)
{  
  char d = buf[digit];
  uint8_t f = digitFont[d];
  int p = pins[seg];
  if (f & mask) {
    digitalWrite (p, 1);
  }
  delay (1);
  digitalWrite (p, 0);
}

void BareLeds::loop() {
  int i, d, mask;
  /*
  // Do both digits at once, but each segment at a time
  for (i=0, mask=1; i<8; i++, mask <<= 1) {
    digitalWrite (pins[i],1);
    if (digitFont[buf[0]]&mask) digitalWrite (coms[0],0);
    if (digitFont[buf[1]]&mask) digitalWrite (coms[1],0);
    delay (3);
    digitalWrite (pins[i],0);
    digitalWrite (coms[0],1);
    digitalWrite (coms[1],1);
  }
  */

  // Do each digit all at once 
  for (d=0; d<2; d++) {
    digitalWrite (coms[d],1^0); // Transistors, high to drive low
    for (i=0, mask=1; i<8; i++, mask <<= 1) {
     if (digitFont[buf[d]]&mask) digitalWrite (pins[i],1);
    }
    delay (10);
    for (i=0, mask=1; i<8; i++, mask <<= 1) {
      digitalWrite (pins[i],0);
    }
    digitalWrite (coms[d],1^1); // Transistors, high to drive low
  }
}

void BareLeds::show (int x)
{
  buf[0] = (x / 10) % 10;
  buf[1] = x % 10;
}
