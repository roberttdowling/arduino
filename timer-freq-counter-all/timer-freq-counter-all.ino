/*
This runs on both my atmega2560s and the 5v pro micro 32u4
----------------------------------------------------------
  Pin 47 on the atmega256
  Pin 4 worked on the 5v pro micro

I could not get the 3v pro micro to register any frequency

The code says pin 11 is to be used on the pro micro.  Maybe pin 4 is
just coupling noise?
*/

#include "FreqCount.h"

void setup() {
  Serial.begin(115200);       
  Serial.println("Frequency Counter");
  FreqCount.begin(1000);
}

void loop() {
  if (FreqCount.available()) {
    unsigned long count = FreqCount.read();
    Serial.print ("Frequency: ");
    Serial.println(count);
  }
  
  // let serial stuff finish
  delay(200);
}
