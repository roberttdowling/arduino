#define PIN 13 // Blue LED

void setup() {
  pinMode (PIN, OUTPUT);
  pinMode (2, OUTPUT);
  Serial.begin(115200);
}

void toggleLED() {
  digitalWrite(PIN, 1^digitalRead (PIN));
  digitalWrite(2, 1^digitalRead (2));
}

void loop () {
  toggleLED ();
  delay (1000);
}
  
