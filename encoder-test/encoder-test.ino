const int ia=0;
const int ib=1;
const int ic=2;

const int pins[3] = {2, 3, 4};
int last[3];
int good[3];
unsigned long t[3];
int state;

char buf[20];

const int count_lo = 10;
const int count_hi = 100;
int count;

void setup() {
  Serial.begin (115200);
  Serial.print ("Hello World\n");
  int t0 = millis ();
  for (int i=0; i<3; i++) {
    pinMode (pins[i], INPUT_PULLUP);
    last[i] = good[i] = digitalRead (pins[i]);
    t[i] = t0;
  }
  count = 50 * 4;
  state = good[ia] | good[ib]*2;
}

const char step[4][4] = {
    // 00 01 10 11
     { 0,-1, 1, 0, }, // 00
     { 1, 0, 0,-1, }, // 01
     {-1, 0, 0, 1, }, // 10
     { 0, 1,-1, 0, }, // 11
};

void loop() {
  int now = millis ();
  for (int i=0; i<3; i++) {
    int x = digitalRead (pins[i]);
    if (x==last[i]) {
      if (now > t[i]+1)
        good[i] = x;
    } else {
      t[i] = now;
    }
    last[i] = x;
  }
  int nextstate = good[ia] | good[ib]*2;
  count += step[state][nextstate];
  state = nextstate;

  if (count > count_hi * 4)
    count = count_hi * 4;
  if (count < count_lo * 4)
    count = count_lo * 4;
  /*
  sprintf (buf, "%d%d%d", last[ia], last[ib], last[ic]);
  Serial.print (buf);
  sprintf (buf, "%d%d%d", good[ia], good[ib], good[ic]);
  Serial.println (buf);
  */
  Serial.print(good[ic]); Serial.print(' ');
  Serial.println (count/4);
}

