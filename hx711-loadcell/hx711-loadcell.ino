const int CLK = A1;
const int DOUT = A0;

void setup() {
  pinMode (CLK, OUTPUT);
  pinMode (DOUT, INPUT);
  Serial.begin(115200);
}

const long GAIN = 217;
const long OFFSET = 488;

void loop() {
  while (digitalRead (DOUT))
    ;
  long x = 0;
  for (int i=0; i<24; i++) {
    digitalWrite (CLK, 1);
    digitalWrite (CLK, 0);
    x = (x<<1) | digitalRead (DOUT);
  }
  // End conversion with 25th clock
  digitalWrite (CLK, 1);
  digitalWrite (CLK, 0);
  // Sign extend the 24 bit result
  x <<= 8;
  x >>= 8;
  Serial.println (x/GAIN-OFFSET);
}

