struct front_panel {
  uint8_t horizontal_rate;
  uint8_t horizontal_offset;
  uint8_t trigger_level;
  uint8_t other_level_lo;
  uint8_t other_level_hi;
  uint8_t hr_button;
  uint8_t ho_button;
  uint8_t trig_auto_switch;
  uint8_t trig_slope_switch;
  uint8_t trig_ch_button;
  uint8_t other_button;
  uint8_t run_stop_button;
  uint8_t any_change;
  uint8_t end_of_record; // Always 255
};


