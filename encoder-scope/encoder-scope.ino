// -*- mode: c++; c-basic-offset: 2; -*-

#include "front_panel.h"

#define DEBUG 0

#if DEBUG
#define Uart Serial
#else
#define Uart Serial1
#endif
#define BAUD 115200
#define PACE_MS 50

#define LO(X) ((X) & 127)
#define HI(X) (((X) >> 7) & 127)

const int CONTROL = A3;

const int PINS = 15;
const int pins[] = {2, 3, 4, 5, 6, 7, 8, 9, A2, A1, A0, 15, 14, 16, 10};
int last[PINS];
int good[PINS];
unsigned long t[PINS];

const int ENCODERS = 4;
struct encoder {
  int a, b; // Indices into pins[]
  int count_lo, count_hi;
  int count;
  int state;
};
struct encoder encoders[] = { // indices into pins[]
  { 0, 1,   1, 102 },          // Horizontal Rate: pins 2,3
  { 3, 4,   0, 100 },         // Horizontal offset: pins 5,6
  { 8, 9,   1, 25 },          // Trigger level: pins A2, A1
  { 11, 12, 0, 319 },          // Other: pins 15, 14
};  

const int SWITCHES = PINS - 2 * ENCODERS;
int switches[SWITCHES] { 2, 5, 6, 7, 10, 13, 14 }; // Indices into pins[]

char buf[20];

void setup() {
  Uart.begin (BAUD);
  Uart.print ("Scope\r\n");
  int t0 = millis ();
  for (int i=0; i<PINS; i++) {
    pinMode (pins[i], INPUT_PULLUP);
    last[i] = good[i] = digitalRead (pins[i]);
    t[i] = t0;
  }
  for (int i=0; i<ENCODERS; i++) {
    struct encoder *e = &encoders[i];
    e->count = 4*((e->count_lo + e->count_hi)/2);
    e->state = good[e->a] | good[e->b]*2;
  }
  pinMode (CONTROL, INPUT);
}

const char step[4][4] = {
    // 00 01 10 11
     { 0,-1, 1, 0, }, // 00
     { 1, 0, 0,-1, }, // 01
     {-1, 0, 0, 1, }, // 10
     { 0, 1,-1, 0, }, // 11
};

unsigned long t0 = millis ();
struct front_panel fp0;

void loop() {
  unsigned long now = millis ();
  for (int i=0; i<PINS; i++) {
    int x = digitalRead (pins[i]);
    if (x==last[i]) {
      if (now > t[i]+1)
        good[i] = x;
    } else {
      t[i] = now;
    }
    last[i] = x;
  }
  for (int i=0; i<ENCODERS; i++) {
    struct encoder *e = &encoders[i];
    int nextstate = good[e->a] | good[e->b]*2;
    e->count += step[e->state][nextstate];
    e->state = nextstate;
    if (e->count > e->count_hi * 4)
      e->count = e->count_hi * 4;
    if (e->count < e->count_lo * 4)
      e->count = e->count_lo * 4;
  }
  if (digitalRead (CONTROL)) {
    if (now > t0 + PACE_MS) {
      t0 = now;
#if DEBUG
      for (int i=0; i<ENCODERS; i++) {
        struct encoder *e = &encoders[i];
        sprintf (buf, "%d ", e->count/4);
        Uart.print (buf);
      }
      for (int i=0; i<SWITCHES; i++) {  
        sprintf (buf, "%d ", good[switches[i]]);
        Uart.print (buf);
      }
      Uart.print ("\r\n");
#else
      struct front_panel fp;
      fp.horizontal_rate = encoders[0].count/4;
      fp.horizontal_offset = encoders[1].count/4;
      fp.trigger_level = encoders[2].count/4;
      fp.other_level_lo = LO(encoders[3].count/4);     
      fp.other_level_hi = HI(encoders[3].count/4);
      fp.hr_button = good[switches[0]];
      fp.ho_button = good[switches[1]];
      fp.trig_auto_switch = good[switches[2]];
      fp.trig_slope_switch = good[switches[3]];
      fp.trig_ch_button = good[switches[4]];
      fp.other_button = good[switches[5]];
      fp.run_stop_button = good[switches[6]];
      fp.end_of_record = 255; // Always 255
      fp.any_change =
        fp.horizontal_rate != fp0.horizontal_rate ||
        fp.horizontal_offset != fp0.horizontal_offset  ||
        fp.trigger_level != fp0.trigger_level ||
        fp.other_level_lo !=  fp0.other_level_lo ||
        fp.other_level_hi !=  fp.other_level_hi ||
        fp.hr_button !=  fp0.hr_button  ||
        fp.ho_button !=  fp0.ho_button ||
        fp.trig_auto_switch != fp0.trig_auto_switch ||
        fp.trig_slope_switch !=  fp0.trig_slope_switch ||
        fp.trig_ch_button != fp0.trig_ch_button ||
        fp.other_button != fp0.other_button ||
        fp.run_stop_button != fp0.run_stop_button;
      Uart.write((char *)&fp, sizeof(fp));
      fp0 = fp;
#endif
    }
  }
}

