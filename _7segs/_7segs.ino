// -*- mode: c++; c-basic-offset: 2; -*-

#include "Wire.h" // i2c driver

#define HT16K33_BLINK_CMD 0x80
#define HT16K33_BLINK_DISPLAYON 0x01
#define HT16K33_BLINK_OFF 0
#define HT16K33_BLINK_2HZ  1
#define HT16K33_BLINK_1HZ  2
#define HT16K33_BLINK_HALFHZ  3

#define HT16K33_CMD_BRIGHTNESS 0xE0
#define HT16K33_CMD_OSC_ON 0x21

int i2c_addr = 0x70;

void setup() {
  // put your setup code here, to run once:
  Wire.begin();

  // turn on oscillator
  Wire.beginTransmission(i2c_addr);
  Wire.write(HT16K33_CMD_OSC_ON);  
  Wire.endTransmission();

  // Turn off blinking, but turn display on
  Wire.beginTransmission(i2c_addr);
  Wire.write(HT16K33_BLINK_CMD | HT16K33_BLINK_DISPLAYON | (0 << 1)); 
  Wire.endTransmission();

  // Pick brightness from 0 to 15, 0 is dim, 15 is bright
  Wire.beginTransmission(i2c_addr);
  Wire.write(HT16K33_CMD_BRIGHTNESS | 5*15/15);
  Wire.endTransmission();  
}

const int digitAddr[4] = { 0, 1, 3, 4 };
const int colonAddr = 2;

void setDigit (int addr, int mask)
{
      Wire.beginTransmission(i2c_addr);
      // Put cursor at digit
      Wire.write((uint8_t)0x00 | (addr << 1));
      Wire.write (~mask);
      Wire.endTransmission(); 
}

void loop() {
  // digits 0, 1, 3, 4 are 4 digits.  Digit 2 is colon and dots
  for (int digit=0; digit<4; digit++) {
    for (int seg=0; seg<7; seg++) {
      setDigit (digitAddr[digit], 1 << seg);
      setDigit (colonAddr, 1 << seg);
      delay (500);
    }
  }
}
