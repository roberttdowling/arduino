// Only for ATMega2560 board (Pins mostly)

void setup() {
  // Strangely close to 1KHz on Pin 9, Timer 2
  pinMode(9, OUTPUT);
  TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM21) | _BV(WGM20); // 1010-0011
  TCCR2B = _BV(CS22);
  OCR2A = 180; // Not needed
  OCR2B = 127; // Position of Pwm.  Out of 256, 50% duty

/*
   // Timer 1
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  OCR1A = 180; // 31250/100; // compare match register 16MHz/256/2Hz
  OCR1B = 127; //OCR1A/2; // 5000/1000;
  TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM11) | _BV(WGM10); // 1010-0011
  TCCR1B = _BV(CS11) | _BV(CS10); // _BV(CS12);
  //TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM11) | _BV(WGM10);
  //TCCR1B |= (1 << WGM12); // CTC mode
  //TCCR1B |= (1 << CS12); // 256 prescaler
  // TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
*/
}

void loop() {
}
