// -*- mode: c++; c-basic-offset: 2; -*-
#include "Arduino.h"
#include "Thermometer.h"

// const int ADC0 = A0;
// const int ADCMAX = 1023;
// const float R = 14800;
// const float T0 = 10000;
const float V = 5.0; // Cancels, so it does not matter

// From data sheet:
const float A = 3.354016e-3;
const float B = 2.569850e-4;
const float C = 2.620131e-6;
const float D = 6.383091e-8;

Thermometer::Thermometer (int adcPin, float r, float t0, int adcmax) :
  ADC0(adcPin), R(r), T0(t0), ADCMAX(adcmax)
{
  pinMode (ADC0, INPUT);
}

float Thermometer::readTempC () {
  int adc = analogRead (ADC0);
  float v = ADC * V / ADCMAX;
  float r = V*R/v-R;
  float l = log(r/T0);
  float k = 1/(A + B*l + C*l*l + D*l*l*l);
  float c = k-273.15;
  return c;
}

float Thermometer::readTempF () {
  float c = readTempC ();
  float f = 32+9*c/5;
  return f;
}

