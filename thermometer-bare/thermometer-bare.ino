// -*- mode: c++; c-basic-offset: 2; -*-
#include "Thermometer.h"
#include "BareLeds.h"

Thermometer therm(A0, 14800, 10000-400); // T0=10000, ADCMAX=1023
BareLeds l(5, 6, 8, 9, 4, 2, 3, 7, A3, A2);

int n;

void setup() {
  n = 0;
}

void loop() {
  n++;
  if ((n&31) == 0) {
    float f = therm.readTempF ();
    int temp = (int)f;
    l.show (temp);
  }
  l.loop ();
}
