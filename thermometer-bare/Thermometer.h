// -*- mode: c++; c-basic-offset: 2; -*-
class Thermometer {
public:
  Thermometer (int adcPin, float r, float t0=10000, int adcmax=1023);
  float readTempC ();
  float readTempF ();
private:
  int ADC0; // ADC Pin to read from
  float T0; // Nomial resistance of Thermistor
  float ADCMAX; // Max ADC reading
  float R; // Fixed resistance in series to ground
};
