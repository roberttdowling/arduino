#include "pt.h"

static struct pt pt1, pt2, pt3, pt4;

#define PIN 13 // Blue LED

void setup() {
  pinMode (PIN, OUTPUT);
  Serial.begin(115200);
  PT_INIT(&pt1);
  PT_INIT(&pt2);
}

void toggleLED() {
  digitalWrite(PIN, 1^digitalRead (PIN));
}

/* This function toggles the LED after 'interval' ms passed */
static int protothread1(struct pt *pt, int interval) {
  static unsigned long timestamp = 0;
  PT_BEGIN(pt);
  while(1) { // never stop 
    /* each time the function is called the second boolean
    *  argument "millis() - timestamp > interval" is re-evaluated
    *  and if false the function exits after that. */
    PT_WAIT_UNTIL(pt, millis() - timestamp > interval );
    timestamp = millis(); // take a new timestamp
    toggleLED();
  }
  PT_END(pt);
}

int j = 0, k=0;

static int protothread2(struct pt *pt, int interval) {
  static unsigned long timestamp = 0;
  PT_BEGIN(pt);
  while(1) {
    PT_WAIT_UNTIL(pt, millis() - timestamp > interval );
    timestamp = millis();
    j++;
  }
  PT_END(pt);
}

static int protothread3(struct pt *pt, int interval) {
  static unsigned long timestamp = 0;
  PT_BEGIN(pt);
  while(1) {
    PT_WAIT_UNTIL(pt, millis() - timestamp > interval );
    timestamp = millis();
    k++;
  }
  PT_END(pt);
}

static int protothread4(struct pt *pt) {
  static unsigned long timestamp = 0;
  PT_BEGIN(pt);
  while(1) {
    PT_WAIT_UNTIL(pt, millis() - timestamp > 1000);
    timestamp = millis();
    static char buf[256];
    sprintf (buf, "j=%d k=%d", j, k);
    Serial.println (buf);
  }
  PT_END(pt);
}

void loop() {
  protothread1(&pt1, 100); // schedule the two protothreads
  protothread2(&pt2, 10); // by calling them infinitely
  protothread3(&pt3, 13); // by calling them infinitely  
  protothread4(&pt4); // by calling them infinitely
}

