// -*- mode: c++; c-basic-offset: 2; -*-
#include <stdio.h>  // sprint
#include <string.h> // memmove, memset

int sprintf_f (char *s, int total, int right, float x)
{
  static const long pow10[] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000};
  // Format string, patched with # of digits
  char fmt[]="%ld.%01ld";

  // Leading -
  char *p = s;
  if (x < 0) {
    x = -x;
    *p++ = '-';
  }
  
  // Whole and fraction parts
  long w = (long) x;
  if (right < 1) { // .0 means no decimal places
    p += sprintf (p, "%ld", w);
  } else {
    long f = pow10[right] * (x-w);
    if (right > 9)
      right = 9;
    fmt[6]='0'+right;
    p += sprintf (p, fmt, w, f);
  }

  // Right justify
  // -8.02  =>  ___-8.02; p-s=5, total=8, d=3
  int d = total - (p-s);
  if (d > 0) {
    memmove (s+d, s, p-s+1); // can't use memcpy
    memset (s, ' ', d);
    return total;
  }
  return p-s;
}

