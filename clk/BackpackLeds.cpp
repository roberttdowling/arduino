// -*- mode: c++; c-basic-offset: 2; -*-
#include "BackpackLeds.h"
#include "Arduino.h"
#include "Wire.h" // i2c driver

#define HT16K33_BLINK_CMD 0x80
#define HT16K33_BLINK_DISPLAYON 0x01
#define HT16K33_BLINK_OFF 0
#define HT16K33_BLINK_2HZ  1
#define HT16K33_BLINK_1HZ  2
#define HT16K33_BLINK_HALFHZ  3

#define HT16K33_CMD_BRIGHTNESS 0xE0
#define HT16K33_CMD_OSC_ON 0x21

static const int digitAddr[4] = { 0, 1, 3, 4 };
static const int colonAddr = 2;

//    01
//  20  02
//    40
//  10  04
//    08

static const uint8_t digitFont[10] = {
  0x3f, 0x06, 0x7f^0x24, 0x7f^0x30, 0x7f^0x19,
  0x7f^0x12, 0x7f^0x02, 0x7, 0x7f, 0x7f^0x10 };

BackpackLeds::BackpackLeds (int addr) : i2c_addr(addr)
{
}

void BackpackLeds::setup ()
{
  Wire.begin();

  // turn on oscillator
  Wire.beginTransmission(i2c_addr);
  Wire.write(HT16K33_CMD_OSC_ON);  
  Wire.endTransmission();

  // Turn off blinking, but turn display on
  Wire.beginTransmission(i2c_addr);
  Wire.write(HT16K33_BLINK_CMD | HT16K33_BLINK_DISPLAYON | (0 << 1)); 
  Wire.endTransmission();

  // Pick brightness from 0 to 15, 0 is dim, 15 is bright
  Wire.beginTransmission(i2c_addr);
  Wire.write(HT16K33_CMD_BRIGHTNESS | 0*15/15);
  Wire.endTransmission();  

  for (int i=0; i<5; i++) {
    setDigitRaw (i, 0);
  }
}

void BackpackLeds::setDigitRaw (int posn, int mask)
{
  Wire.beginTransmission(i2c_addr);
  // Put cursor at digit
  Wire.write((uint8_t)0x00 | (posn << 1));
  Wire.write (mask);
  Wire.endTransmission(); 
}

void BackpackLeds::setDigit (int digitIx, char digit)
{
  Wire.beginTransmission(i2c_addr);
  // Put cursor at digit
  Wire.write((uint8_t)0x00 | (digitAddr[digitIx] << 1));
  switch (digit) {
  case 0: case 1: case 2: case 3: case 4:
  case 5: case 6: case 7: case 8: case 9:
    Wire.write (digitFont[digit]);
    break;
  case '0': case '1': case '2': case '3': case '4':
  case '5': case '6': case '7': case '8': case '9':
    Wire.write (digitFont[digit-'0']);
    break;
  default:
  case ' ':
    Wire.write (0);
    break;
  }
  Wire.endTransmission(); 
}

