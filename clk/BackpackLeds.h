// -*- mode: c++; c-basic-offset: 2; -*-
class BackpackLeds {
public:
  BackpackLeds (int addr = 0x70); // Can be 0x70..0x7f  Adafruit backpack
  void setup ();
  void setDigit (int digitIx, char digit);
  void setDigitRaw (int posn, int mask);
private:
  int i2c_addr;
};

