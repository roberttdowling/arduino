// -*- mode: c++; c-basic-offset: 2; -*-
#include "BackpackLeds.h"

BackpackLeds l;

void setup() {
  l.setup ();
}

static int min = 45, hr = 15;

void loop() {
  int tens = hr / 10;
  int ones = hr % 10;
  l.setDigit (0, tens);
  l.setDigit (1, ones);
  tens = min / 10;
  ones = min % 10;
  l.setDigit (2, tens);
  l.setDigit (3, ones);
  delay (30*1000);
  delay (30*1000);
  min++;
  if (min >=60) {
    min = 0;
    hr++;
    if (hr >= 24) {
      hr = 0;
    }
  }
}

