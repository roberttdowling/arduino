// -*- mode: c++; c-basic-offset: 2; -*-
#include "Thermometer.h"
#include "BackpackLeds.h"

Thermometer therm(A0, 9840, 10000-600); // T0=10000, ADCMAX=1023
BackpackLeds l;

void setup() {
  l.setup ();
}

void loop() {
  float f = therm.readTempF();
  int i = (int)f;
  int tens = i / 10;
  int ones = i % 10;
  l.setDigit (0, tens);
  l.setDigit (1, ones);
  int tenths = (f-i)*10;
  if (tenths > 9) tenths = 9;
  l.setDigit (2, tenths);
  delay (1000);
}
