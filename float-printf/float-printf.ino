// -*- mode: c++; c-basic-offset: 2; -*-

int sprintf_f (char *s, int total, int right, float x)
{
  static const long pow10[] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000};
  // Format string, patched with # of digits
  char fmt[]="%ld.%01ld";
  fmt[6]='0'+right;

  // Leading -
  char *p = s;
  if (x < 0) {
    x = -x;
    *p++ = '-';
  }
  
  // Whole and fraction parts
  long w = (long) x;
  long f = pow10[right] * (x-w);
  p += sprintf (p, fmt, w, f);

  // Right justify
  // -8.02  =>  ___-8.02; p-s=5, total=8, d=3
  int d = total - (p-s);
  if (d > 0) {
    memmove (s+d, s, p-s+1); // can't use memcpy
    memset (s, ' ', d);
    return total;
  }
  return p-s;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin (115200);
  static char buf[128];
  float i;
  for (i=-2; i<=2; i+=1.0/8) {
    int w, f;
    sprintf_f (buf, 8,2, i);
    Serial.println(buf);
  }
  sprintf_f (buf, 8,2, 12345.67);
  Serial.println(buf);
}

void loop() {
  // put your main code here, to run repeatedly:
}
