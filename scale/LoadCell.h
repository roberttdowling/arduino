// -*- mode: c++; c-basic-offset: 2; -*-

class LoadCell {
public:
  LoadCell (int clkPin, int doutPin);
  void poll (); // wait for next data and update readings
  long raw (); // read raw value
  float grams (); // read grams
  float toGrams (long raw); // convert another reading to grams
  long toADC (float grams); // convert another reading of grams to ADC
  void setGain (float gain); // calibrate
  void setOffset (long raw); // calibrate
private:
  int CLK, DOUT; // pins
  float GAIN;
  long OFFSET;
  long lastRawRead; // last readings
  float lastGRead;
};
