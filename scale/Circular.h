#ifndef _CIRCULAR_H
#define _CIRCULAR_H

#define _n 200

class Circular {
public:
	Circular (int n);
	~Circular ();
	float next (float x);
	void init ();
private:
	int _i;
	float _buf[_n];
};


#endif // _CIRCULAR_H
