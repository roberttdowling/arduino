// -*- mode: c++; c-basic-offset: 2; -*-

#include "LoadCell.h"
#include "Arduino.h"

LoadCell::LoadCell (int clkPin, int doutPin) :
  CLK(clkPin), DOUT(doutPin),
  GAIN(217.0), OFFSET(122220)
{
  pinMode (CLK, OUTPUT);
  pinMode (DOUT, INPUT);
  lastRawRead = OFFSET;
  lastGRead = 0;
}

void LoadCell::poll() {
  while (digitalRead (DOUT))
    ;
  long x = 0;
  for (int i=0; i<24; i++) {
    digitalWrite (CLK, 1);
    digitalWrite (CLK, 0);
    x = (x<<1) | digitalRead (DOUT);
  }
  // End conversion with 25th clock
  digitalWrite (CLK, 1);
  digitalWrite (CLK, 0);
  // Sign extend the 24 bit result
  x <<= 8;
  x >>= 8;
  lastRawRead = x;
  lastGRead = toGrams(x);
}

long LoadCell::raw () { return lastRawRead; }
float LoadCell::grams () { return lastGRead; }
float LoadCell::toGrams (long raw) { return (float)(raw-OFFSET)/GAIN; }
long LoadCell::toADC (float grams) { return grams*GAIN+OFFSET; }
void LoadCell::setGain (float gain) { GAIN = gain; }
void LoadCell::setOffset (long raw) { OFFSET = raw; }
