#ifndef FEMV_H_
#define FEMV_H_

#include <stdint.h>

class FEmv
{
public:
	FEmv(float alpha);

	// Reset mean and variance back to 0 or something else
	void init (float x = 0.0, float v = 0.0);
	// Process next input point x, return current variance
	float filter(float x);
	// Return last computed variance
	float var();
	// Return last computed mean
	float mean();
	// Set alpha
	void setAlpha (float alpha);
private:
	float _oneminusalpha;
        float _var;
        float _mean;
	float _alpha;
};
#endif
