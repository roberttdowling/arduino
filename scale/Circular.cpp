#include "Circular.h"

Circular::Circular (int n)
{
	_i = 0;
//	_buf = new float[_n];
	init ();
}

Circular::~Circular ()
{
//	delete [] _buf;
}

float Circular::next (float x)
{
	float y = _buf[_i];
	_buf[_i] = x;
	_i = (_i+1) % _n;
	return y;
}

void Circular::init ()
{
	for (int i=0; i<_n; i++)
		_buf[i] = 0;
}
