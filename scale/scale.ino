// -*- mode: c++; c-basic-offset: 2; -*-
#include <LiquidCrystal.h>
#include "LoadCell.h"
#include "sprintf_f.h"
#include "FEmv.h"
#include "LeastSquares.h"

const int LED = 10;

// LCD Pins
const int RS = 9;  // 0=Instr, 1=Data
const int RW = 8; // 0=Write, 1=Read
const int E  = 7;  // 450ns pos pulse to start transfer
const int D4 = 6;
const int D5 = 5;
const int D6 = 4;
const int D7 = 3;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(RS, RW, E, D4, D5, D6, D7);

// Lode Cell
LoadCell lc(A1, A0); // Clk=A1, DOUT=A0
const int SIGMA = 22.22; // Measured std on raw ADC when resting
const int THRESHOLD = 4*SIGMA; // Arbitrary "change" threshold on std

// Button
const int BUTTON_TARE = 2; // Tare
const int BUTTON_DISP = 1; // Display mode

// Guts of Scale
const float ALPHA = 1.0/300;
const int NUM_ALPHAS = 100;
float tare;
float drift0;

FEmv e(ALPHA);
LeastSquares fit;

bool acceptTare, requestTare, stable;
int resetMean;

void light (bool on) {
  digitalWrite (LED, on ? 0 : 1);
}

void setup() {
  pinMode (BUTTON_TARE, INPUT_PULLUP);
  pinMode (BUTTON_DISP, INPUT_PULLUP);
  Serial.begin(115200);
  pinMode (LED, OUTPUT);
  light (true);

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // Prime pump
  lc.poll ();
  resetMean = NUM_ALPHAS;
  requestTare = true;
  acceptTare = false;
  stable = false;
}

void loop() {
  lc.poll (); // this blocks until ADC is ready: 11.0Hz
  long x = lc.raw();

  // Jump to a new mean if scale moved enough (about 1 g)
  if (abs(x-e.mean()) > THRESHOLD) {
    resetMean = NUM_ALPHAS;
  }

  // Two basic states: stable and changing
  if (resetMean) {
    // Unstable until resetMean goes back to 0
    resetMean--;
    // Adapt alpha; note: final setting puts it back to ALPHA
    // 1, 1, 1, 1, 1/2, 1/3, 1/4, ... 1/96, ALPHA
    int i = NUM_ALPHAS-3 - resetMean;
    if (i<1) i = 1;
    e.setAlpha(resetMean ? 1.0/i : ALPHA);
  }

  // Filter point
  e.filter(x);
  float mx = e.mean ();

  if (!resetMean && !stable) {
    // This is the change to stable
    fit.init ();
    drift0 = mx;
  }
  stable = !resetMean;

  // User tare
  if (!digitalRead (BUTTON_TARE)) {
      requestTare = true;
  }

  if (stable && requestTare) {
    // Allow tare
    requestTare = false;
    acceptTare = true;
  }

  // Tare
  if (acceptTare) {
    acceptTare = false;
    lc.setOffset(mx);
    drift0 = tare = mx;
    fit.init ();
    e.init(mx, e.var());
  }

  if (stable) {
    // Accumulate drift data
    fit.add ((float)millis(), mx);
    mx = drift0;
  } else {
    mx = drift0 + mx - fit.y(millis());
  }

  light (!stable);

  float g = lc.toGrams(mx);

  static char buf[128], *s;

  if (digitalRead (BUTTON_DISP)) {
  // Mean ADC, resetMean, drift0
  long meanADC = (long) mx;
  s = buf;
  s += sprintf (s, "%6ld%3d %6ld", meanADC, resetMean, (long)drift0);
  lcd.setCursor(0, 0);
  lcd.print(buf);
  //  Serial.print (buf);
    
  // n, m, b
  // Grams, drift
  s = buf;
  s += sprintf_f (s, 8,2, g);
  s += sprintf_f (s, 8,2, fit.y(millis()) - drift0); // drift);
  lcd.setCursor(0, 1);
  lcd.print(buf);
  //  Serial.print (buf);

  } else {

  // n, m, b
  s = buf;
  s += sprintf_f (s, 8,5, fit.m());
  s += sprintf (s, "%8ld", (long) fit.n());
  lcd.setCursor(0, 0);
  lcd.print(buf);
  //  Serial.println (buf);

  // Grams, drift
  s = buf;
  s += sprintf_f (s, 8,2, mx - drift0);
  s += sprintf_f (s, 8,2, fit.y(millis()) - drift0); // drift);
  lcd.setCursor(0, 1);
  lcd.print(buf);
  //  Serial.print (buf);
  }
}
