// Exponential Moving Variance: Floating Point Implementation
//
// (C) Copyright 2016 Robert Dowling.
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
// From
// http://www-uxsup.csx.cam.ac.uk/~fanf2/hermes/doc/antiforgery/stats.pdf
//
// diff := x - mean
// incr := alpha * diff
// mean := mean + incr
// variance := (1 - alpha) * (variance + diff * incr)
//
// Usage:
// 
// Emv e = new Emv(1/4.0);
// float y = e->filter (x);
// float v = e->var(); // Same result as y, using last saved variance
// float m = mean ();
// float s = sqrt(v); // standard deviation

#include "FEmv.h"

FEmv::FEmv(float alpha)
{
  setAlpha (alpha);
  init ();
}

void FEmv::init (float x, float v)
{
  _mean = x;
  _var = v;
}

void FEmv::setAlpha (float alpha)
{
  _alpha = alpha;
  _oneminusalpha = 1-alpha;
}

float FEmv::filter(float x)
{
  // Exponential Moving Variance
  float diff = x - _mean;
  float incr = diff * _alpha;
  _mean += incr;
  _var = _oneminusalpha * (_var + diff * incr);

  return _var;
}

float FEmv::var() { return _var; }
float FEmv::mean() { return _mean; }
