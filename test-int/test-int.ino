// -*- mode: c++; c-basic-offset: 2; -*-

#define MEMSIZE 1024
char printbuf[MEMSIZE+4];

void setup()
{
  Serial.begin(115200);
}

void loop()
{
  float xl=1.2, xm=2.5, xh=3.8;
  sprintf (printbuf, "1.2=%d 2.5=%d 3.8=%d", (int)xl, (int)xm, (int)xh);
  Serial.println (printbuf);
  xl=-1.2, xm=-2.5, xh=-3.8;
  sprintf (printbuf, "-1.2=%d -2.5=%d -3.8=%d\n", (int)xl, (int)xm, (int)xh);
  Serial.println (printbuf);
  delay (1000);
}
