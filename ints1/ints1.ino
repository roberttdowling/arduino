const int pin = 2;
volatile long count = 0;

void isr () {
  count++;
}

long t0, t1;

void setup() {
  // put your setup code here, to run once:
  pinMode (pin, INPUT_PULLUP);
  attachInterrupt (digitalPinToInterrupt(pin), isr, RISING); // RISING);
  Serial.begin (115200);
  t0 = millis ();
}

void loop() {
  // put your main code here, to run repeatedly:
  t1 = millis ();
  if (t1 - t0 > 1000) {
    t0 = t1;
    Serial.println (count);
    count = 0;
  }
}
