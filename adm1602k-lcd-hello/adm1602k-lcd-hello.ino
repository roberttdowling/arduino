#include <LiquidCrystal.h>

const int RS = 2;  // 0=Instr, 1=Data
const int RW = 3; // 0=Write, 1=Read
const int E  = 4;  // 450ns pos pulse to start transfer
const int D4 = 5;
const int D5 = 6;
const int D6 = 7;
const int D7 = 8;

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(RS, RW, E, D4, D5, D6, D7);

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("hello, world!");
  pinMode (RW, OUTPUT);
  
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(millis() / 1000);
}

/*
const int CLK = A1;
const int DOUT = A0;


void setup() {
  pinMode (CLK, OUTPUT);
  pinMode (DOUT, INPUT);

  pinMode (RS, OUTPUT);
  pinMode (RW, OUTPUT);
  pinMode (E, OUTPUT);
  Serial.begin(115200);
}

// Display adm1602k is 16x2


void loop() {
  

const long GAIN = 217;
const long OFFSET = 488;

void nullloop() {
  while (digitalRead (DOUT))
    ;
  long x = 0;
  for (int i=0; i<24; i++) {
    digitalWrite (CLK, 1);
    digitalWrite (CLK, 0);
    x = (x<<1) | digitalRead (DOUT);
  }
  // End conversion with 25th clock
  digitalWrite (CLK, 1);
  digitalWrite (CLK, 0);
  // Sign extend the 24 bit result
  x <<= 8;
  x >>= 8;
  Serial.println (x/GAIN-OFFSET);
}

*/
